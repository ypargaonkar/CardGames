import java.util.ArrayList;

public class Pack {
	
	public int JOKER_SUIT = 5 ; 
	public int JOKER_RANK = 14 ; 
	public ArrayList<Card> cards ; 
	
	public Pack() {
		cards = new ArrayList<Card>() ; 
		cards.addAll(basePack()) ;  
	}
	
	public Pack(int njokers) {
		cards = new ArrayList<Card>() ; 
		cards.addAll(basePack()) ; 
		Card joker = new Card(JOKER_SUIT,JOKER_RANK) ; 
		for(int i = 0 ; i < njokers ; i++) {
			cards.add(joker) ; 
		}
	}
	
	private ArrayList<Card> basePack(){
		ArrayList<Card> ret = new ArrayList<Card>() ; 
		for(int i = 1 ; i <= 13 ; i++) {
			for(int j = 1 ; j <= 4 ; j++) {
				ret.add(new Card(i,j)) ; 
			}
		}
		return ret ; 
	}
	
	
	
	
}
